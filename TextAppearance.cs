﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp
{
    class TextAppearance
    {
        public const int APP_WIDTH = 48;
        public const char BORDER_CHAR = '=';
        public const ConsoleColor BASE_COLOR = ConsoleColor.Gray;
        public const ConsoleColor SECOND_COLOR = ConsoleColor.DarkBlue;

        public static string DisplayWithBorders(string[] texts)
        {
            DisplayBorder();
            foreach (string text in texts)
                Console.WriteLine(WrapText(text));

            DisplayBorder();
            Console.Write("Twój wybór: ");
            string choice = Console.ReadLine();
            Console.Clear();

            return choice.ToUpper().Trim();
        }

        public static void DisplayWithColor(string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ForegroundColor = BASE_COLOR;

            if(color == ConsoleColor.Red)
            {
               System.Media.SystemSounds.Hand.Play();
            }
            else if(color == ConsoleColor.Green)
            {
                System.Media.SystemSounds.Exclamation.Play();
            }
        }

        public static void DisplayBorder()
        {
            string border = "";
            for (int i = 0; i < APP_WIDTH; i++)
                border += BORDER_CHAR;

            DisplayWithColor(border, SECOND_COLOR);
        }

        public static string WrapText(string text)
        {
            if (text.Length <= APP_WIDTH)
                return text;

            int lineStartIndex = 0;
            int previousSpaceIndex = 0;
            int searchingIndex = -1;

            do
            {
                searchingIndex = text.IndexOf(" ", searchingIndex + 1);

                if (searchingIndex != -1)
                {
                    if (searchingIndex - lineStartIndex >= APP_WIDTH)
                    {
                        text = text.Substring(0, previousSpaceIndex) + "\n" + text.Substring(previousSpaceIndex + 1);
                        lineStartIndex = previousSpaceIndex + 1;
                        previousSpaceIndex = searchingIndex;
                    }
                    else
                        previousSpaceIndex = searchingIndex;
                }
            } while (searchingIndex != -1);

            return text;
        }

        public static string ReadPassword(char mask)
        {
            const int ENTER = 13, BACKSP = 8, CTRLBACKSP = 127;
            int[] FILTERED = { 0, 27, 9, 10 /*, 32 space, if you care */ }; // const

            var pass = new Stack<char>();
            char chr = (char)0;

            while ((chr = Console.ReadKey(true).KeyChar) != ENTER)
            {
                if (chr == BACKSP)
                {
                    if (pass.Count > 0)
                    {
                        Console.Write("\b \b");
                        pass.Pop();
                    }
                }
                else if (chr == CTRLBACKSP)
                {
                    while (pass.Count > 0)
                    {
                        Console.Write("\b \b");
                        pass.Pop();
                    }
                }
                else if (FILTERED.Count(x => chr == x) > 0) { }
                else
                {
                    pass.Push((char)chr);
                    Console.Write(mask);
                }
            }

            Console.WriteLine();
            return new string(pass.Reverse().ToArray());
        }
    }
}
