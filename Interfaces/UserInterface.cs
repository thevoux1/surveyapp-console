﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Interfaces
{
    class UserInterface
    {
        public User LoggedUser { get; set; }
        private ObservableCollection<Survey> surveys;

        public UserInterface(User user)
        {
            LoggedUser = user;
        }

        public void Show()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine("Twoje ankiety:");
            surveys = Program.Hibernate.Query<Survey>("from Survey where User = " + LoggedUser.ID);

            if (surveys.Count == 0)
                Console.WriteLine(TextAppearance.WrapText("Nie posiadasz żadnych ankiet"));
            else
            {
                foreach (Survey survey in surveys)
                    Console.WriteLine(TextAppearance.WrapText((surveys.IndexOf(survey) + 1) + ") " + survey.Name));
            }

            string choice = TextAppearance.DisplayWithBorders(new string[] { "H - zmiana hasła", "N - utwórz ankietę prywatną", "P - utwórz ankietę publiczną", "U - usuń ankietę", "Z - zakończ ankietę", "W - wyloguj się", "Nr ankiety - podgląd wyników" });

            int choiceInt;
            if (choice == "N")
            {
                CreateSurveyInterface createSurveyInterface = new CreateSurveyInterface(LoggedUser, false);
                if(createSurveyInterface.Show())
                    TextAppearance.DisplayWithColor(TextAppearance.WrapText("Pomyślnie dodano prywatną ankietę, kod dostępu został wysłany na Twój adres e-mail"), ConsoleColor.Green);
                else
                    TextAppearance.DisplayWithColor(TextAppearance.WrapText("Pomyślnie zrezygnowano z tworzenia ankiety"), ConsoleColor.Green);

                Show();
            }
            else if (choice == "P")
            {
                CreateSurveyInterface createSurveyInterface = new CreateSurveyInterface(LoggedUser, true);
                if(createSurveyInterface.Show())
                    TextAppearance.DisplayWithColor(TextAppearance.WrapText("Pomyślnie dodano publiczną ankietę"), ConsoleColor.Green);
                else
                    TextAppearance.DisplayWithColor(TextAppearance.WrapText("Pomyślnie zrezygnowano z tworzenia ankiety"), ConsoleColor.Green);

                Show();
            }
            else if (choice == "U")
            {
                Survey surveyToDelete = ShowAllSurveys();
                if (surveyToDelete != null)
                {
                    surveyToDelete.User = null;
                    Program.Hibernate.UpdateData(surveyToDelete);
                    Console.Clear();
                    TextAppearance.DisplayWithColor(TextAppearance.WrapText("Pomyślnie usunięto ankietę"), ConsoleColor.Green);
                    Show();
                }
                else
                {
                    Console.Clear();
                    Show();
                }
            }
            else if (choice == "Z")
            {
                Survey surveyToEnd = ShowAllSurveys();
                if (surveyToEnd != null)
                {
                    Console.Clear();

                    if (surveyToEnd.EndDate < DateTime.Now)
                    {
                        TextAppearance.DisplayWithColor(TextAppearance.WrapText("Ankieta została zakończona już wcześniej"), ConsoleColor.Green);
                    }
                    else
                    {
                        surveyToEnd.EndDate = DateTime.Now;
                        Program.Hibernate.UpdateData(surveyToEnd);
                        TextAppearance.DisplayWithColor(TextAppearance.WrapText("Pomyślnie zakończono ankietę"), ConsoleColor.Green);
                    }

                    Show();
                }
                else
                {
                    Console.Clear();
                    Show();
                }
            }
            else if (choice == "W")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor("Pomyślnie wylogowano", ConsoleColor.Green);
                Program.Main(null);
            }
            else if (choice == "H")
            {
                Console.Clear();
                ManageAccountInterface manageAccountInterface = new ManageAccountInterface(LoggedUser);
                manageAccountInterface.ChangePassword();
                Show();
            }
            else
            {
                if (Int32.TryParse(choice, out choiceInt))
                {
                    if(choiceInt >= 1 && choiceInt <= surveys.Count)
                    {
                        ResultsInterface resultsInterface = new ResultsInterface(surveys[choiceInt - 1]);
                        resultsInterface.Show();
                        Console.Clear();
                        Show();
                    }
                    else
                    {
                        Console.Clear();
                        TextAppearance.DisplayWithColor(TextAppearance.WrapText("Nie istnieje ankieta o takim numerze"), ConsoleColor.Red);
                        Show();
                    }

                    return;
                }

                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Nie rozpoznano opcji"), ConsoleColor.Red);
                Show();
            }
        }

        public Survey ShowAllSurveys()
        {
            if (surveys.Count == 0)
            {
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Aktualnie nie posiadasz żadnych ankiet"), ConsoleColor.Red);
                Show();
                return null;
            }

            TextAppearance.DisplayBorder();
            Console.WriteLine("Twoje ankiety:");
            foreach (Survey survey in surveys)
                Console.WriteLine(TextAppearance.WrapText((surveys.IndexOf(survey) + 1) + ") " + survey.Name));

            Console.WriteLine(TextAppearance.WrapText("P - powrót do panelu użytkownika"));
            TextAppearance.DisplayBorder();
            Console.Write("Numer ankiety: ");
            string choice = Console.ReadLine();

            if(choice.Trim().ToUpper() == "P")
                return null;

            int surveyNumber;
            if (!Int32.TryParse(choice, out surveyNumber))
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Błędny numer ankiety"), ConsoleColor.Red);
                return ShowAllSurveys();
            }
            else
            {
                if (surveyNumber < 1 || surveyNumber > surveys.Count)
                {
                    Console.Clear();
                    TextAppearance.DisplayWithColor(TextAppearance.WrapText("Błędny numer ankiety"), ConsoleColor.Red);
                    return ShowAllSurveys();
                }
                else
                {
                    return surveys[surveyNumber - 1];
                }
            }
        }
    }
}
