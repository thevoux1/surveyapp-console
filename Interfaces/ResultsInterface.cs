﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Interfaces
{
    class ResultsInterface
    {
        public Survey CurrentSurvey { get; set; }

        public ResultsInterface(Survey survey)
        {
            CurrentSurvey = survey;
        }

        public void Show()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Ankieta \"" + CurrentSurvey.Name + "\""));

            ObservableCollection<Question> questions = Program.Hibernate.Query<Question>("from Question where Survey = " + CurrentSurvey.ID);
            foreach(Question question in questions)
            {
                TextAppearance.DisplayBorder();
                Console.WriteLine(TextAppearance.WrapText((questions.IndexOf(question)+1) + ") " + question.Content));

                ObservableCollection<Answer> answers = Program.Hibernate.Query<Answer>("from Answer where Question = " + question.ID);
                int selectionsCounter = 0;
                foreach (Answer answer in answers)
                    selectionsCounter += answer.Selections;

                char answerCounter = 'a';
                foreach (Answer answer in answers)
                {
                    string percent;
                    if (selectionsCounter != 0)
                        percent = "(" + ((float)answer.Selections / selectionsCounter * 100).ToString("0.00") + "%)";
                    else
                        percent = "(0.00%)";

                    if (question.Type == 0)
                        Console.WriteLine(TextAppearance.WrapText("   " + answerCounter++ + ") " + answer.Content + " " + answer.Selections + "/" + selectionsCounter + " " + percent));
                    else
                        Console.WriteLine(TextAppearance.WrapText("   " + answer.Content + " " + answer.Selections + "/" + selectionsCounter + " " + percent));
                }
            }

            string choice = TextAppearance.DisplayWithBorders(new string[] { "P - powrót do konta"});
            if(choice== "P")
            {
                return;
            }
            else
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Nie rozpoznano opcji"), ConsoleColor.Red);
                Show();
            }
        }
    }
}
