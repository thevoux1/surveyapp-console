﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Mail;

namespace surveyapp.Interfaces
{
    class CreateSurveyInterface
    {
        private List<Question> questions;
        private List<Answer> answers;
        public Survey NewSurvey { get; set; }

        public CreateSurveyInterface(User user, bool isPublic)
        {
            questions = new List<Question>();
            answers = new List<Answer>();
            NewSurvey = new Survey();
            NewSurvey.User = user;

            if(!isPublic)
                NewSurvey.AccessKey = AccessKeyCreator();
        }

        public bool Show()
        {
            NewSurvey.Name = ShowNameCreator();
            Console.Clear();
            NewSurvey.StartDate = ShowStartDateCreator();
            Console.Clear();
            NewSurvey.EndDate = ShowEndDateCreator();
            Console.Clear();
            NewSurvey.Description = ShowDescriptionCreator();
            Console.Clear();
            ShowQuestionsCreator();

            if (questions.Count != 0)
            {
                Program.Hibernate.AddSurvey(NewSurvey, questions, answers);
                if (NewSurvey.AccessKey != null)
                    SendEmail(NewSurvey.User.Email, NewSurvey.User.Login, NewSurvey.Name, NewSurvey.AccessKey);

                return true;
            }
            else
                return false;
        }

        public string ShowNameCreator()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Podaj nazwę nowej ankiety"));
            TextAppearance.DisplayBorder();
            Console.Write("Nazwa: ");
            string name = Console.ReadLine();
            if (name.Replace(" ", "") == "")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Nazwa aniekty nie może być pusta"), ConsoleColor.Red);
                return ShowNameCreator();
            }
            else if (name.Length < 10)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Nazwa aniekty nie może być krótsza niż 10 znaków"), ConsoleColor.Red);
                return ShowNameCreator();
            }
            else
                return name;
        }

        public DateTime ShowStartDateCreator()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Ankieta \"" + NewSurvey.Name + "\""));;
            TextAppearance.DisplayBorder();
            Console.WriteLine("Podaj datę rozpoczęcia\nPrzykładowo: 12.10.2020 12:00:00\n'teraz' aby uzupełnić aktualną datą");
            TextAppearance.DisplayBorder();
            Console.Write("Data: ");
            string startDate = Console.ReadLine();
            if (startDate.Replace(" ", "") == "")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Data rozpoczęcia nie może być pusta"), ConsoleColor.Red);
                return ShowStartDateCreator();
            }

            DateTime date;
            if (startDate == "teraz")
            {
                date = DateTime.Now;
            }
            else
            {
                try
                {
                    date = DateTime.ParseExact(startDate, "dd.MM.yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                }
                catch (FormatException)
                {
                    Console.Clear();
                    TextAppearance.DisplayWithColor(TextAppearance.WrapText("Nieprawidłowy format daty"), ConsoleColor.Red);
                    return ShowStartDateCreator();
                }

                if (date < DateTime.Now)
                {
                    Console.Clear();
                    TextAppearance.DisplayWithColor(TextAppearance.WrapText("Data rozpoczęcia musi być datą przyszłą"), ConsoleColor.Red);
                    return ShowStartDateCreator();
                }
            }

            return date;
        }

        public DateTime ShowEndDateCreator()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Ankieta \"" + NewSurvey.Name + "\""));
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Podaj datę zakończenia\nPrzykładowo: 31.10.2020 16:30:00"));
            TextAppearance.DisplayBorder();
            Console.Write("Data: ");
            string startDate = Console.ReadLine();
            if (startDate.Replace(" ", "") == "")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Data zakończenia nie może być pusta"), ConsoleColor.Red);
                return ShowEndDateCreator();
            }

            DateTime date;
            try
            {
                date = DateTime.ParseExact(startDate, "dd.MM.yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (FormatException)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Nieprawidłowy format daty"), ConsoleColor.Red);
                return ShowEndDateCreator();
            }

            if (date < NewSurvey.StartDate)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Data zakończenia musi być większa od daty rozpoczęcia"), ConsoleColor.Red);
                return ShowEndDateCreator();
            }

            return date;
        }

        public string ShowDescriptionCreator()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Ankieta \"" + NewSurvey.Name + "\""));
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Napisz kilka słów o swojej ankiecie (cel, do kogo jest skierowana, przewidywana ilość czasu)"));
            TextAppearance.DisplayBorder();
            Console.Write("Opis: ");
            string desctiption = Console.ReadLine();
            if (desctiption.Replace(" ", "") == "")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Opis aniekty nie może być pusty"), ConsoleColor.Red);
                return ShowDescriptionCreator();
            }
            else if (desctiption.Length < 20)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Opis aniekty nie może być krótszy niż 20 znaków"), ConsoleColor.Red);
                return ShowDescriptionCreator();
            }
            else
                return desctiption;
        }

        public string AccessKeyCreator()
        {
            AccessKeyInterface accessKeyInterface = new AccessKeyInterface();
            ObservableCollection<Survey> surveys = Program.Hibernate.Query<Survey>("from Survey where AccessKey is not null");
            string accessKey;

            bool exists;
            do
            {
                exists = false;
                accessKey = accessKeyInterface.GenerateKey();

                foreach (Survey survey in surveys)
                {
                    if (survey.AccessKey == accessKey)
                    {
                        exists = true;
                        break;
                    }
                }

            } while (exists);

            return accessKey;
        }

        public void ShowQuestionsCreator()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Ankieta \"" + NewSurvey.Name + "\""));

            if (questions.Count != 0)
            {
                TextAppearance.DisplayBorder();
                Console.WriteLine(TextAppearance.WrapText("Dodane pytania:"));

                foreach(Question question in questions)
                    Console.WriteLine(TextAppearance.WrapText((questions.IndexOf(question)+1) + ") " + question.Content));
            }

            string choice = TextAppearance.DisplayWithBorders(new string[] { "0 - Dodaj pytanie zamknięte", "1 - Dodaj pytanie otwarte", "2 - Dodaj pytanie typu (nie)zgadzam się", "W - zakończ dodawanie pytań", "Z - zrezygnuj z tworzenia ankiety" });

            if(choice == "W")
            {
                if(questions.Count == 0)
                {
                    Console.Clear();
                    TextAppearance.DisplayWithColor(TextAppearance.WrapText("Dodaj co najmniej jedno pytanie"), ConsoleColor.Red);
                }
                else
                {
                    Console.Clear();
                    return;
                }
            }
            else if(choice == "Z")
            {
                questions.Clear();
                Console.Clear();
                return;
            }
            else if(choice == "0" || choice == "1" || choice == "2")
            {
                string content = QuestionContentCreator();
                Console.Clear();

                Question question = new Question()
                {
                    Survey = NewSurvey,
                    Content = content,
                    Type = Int32.Parse(choice)
                };

                if(choice == "0")
                {
                    AnswersCreatorInterface answersCreatorInterface = new AnswersCreatorInterface(NewSurvey.Name, question, questions.Count + 1);
                    List<Answer> answersToQuestion = answersCreatorInterface.CreateAnswer();
                    
                    if(answersToQuestion != null)
                    {
                        questions.Add(question);

                        foreach (Answer answer in answersToQuestion)
                            answers.Add(answer);
                    }
                }
                else
                {
                    questions.Add(question);
                }
            }
            else
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Nie rozpoznano opcji"), ConsoleColor.Red);
            }

            ShowQuestionsCreator();
        }

        public string QuestionContentCreator()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Dodawanie pytania #" + (questions.Count + 1)));
            TextAppearance.DisplayBorder();
            Console.Write("Pytanie: ");
            string content = Console.ReadLine();

            if (content.Replace(" ", "") == "")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Treść pytania nie może być pusta"), ConsoleColor.Red);
                return QuestionContentCreator();
            }
            else if (content.Length < 5)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Treść pytania nie może być krótsza niż 5 znaków"), ConsoleColor.Red);
                return QuestionContentCreator();
            }

            return content;
        }

        public void SendEmail(string recipent, string userName, string surveyName, string accessKey)
        {
            try
            {
                var smtpClient = new SmtpClient("smtp.gmail.com")
                {
                    Port = 587,
                    Credentials = new NetworkCredential("noreply.surveyapp@gmail.com", "SecurePassW"),
                    EnableSsl = true,
                };

                smtpClient.Send("noreply.surveyapp@gmail.com", recipent, "Utworzono nową ankietę", "Witaj, " + userName + "!\n\nKod dostępu do Twojej ankiety \"" + surveyName + "\" to " + accessKey + "\nPamiętaj, aby przesłać go tylko powołanym osobom!\n\nPozdrawiamy,\nZespół SurveyApp");
            }
            catch { }
        }
    }
}
