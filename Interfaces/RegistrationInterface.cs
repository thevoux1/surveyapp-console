﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Interfaces
{
    class RegistrationInterface
    {
        public void Show()
        {
            User NewUser = new User();
            string email = ShowEmailCreator();
            if (email == null)
                return;
            else
                NewUser.Email = email;

            Console.Clear();
            string login = ShowLoginCreator();
            if (login == null)
                return;
            else
                NewUser.Login = login;

            Console.Clear();
            string password = ShowPasswordCreator();
            if (password == null)
                return;
            else
                NewUser.Password = password;

            Program.Hibernate.AddData(NewUser);
            SendEmail(NewUser.Email, NewUser.Login);
            Console.Clear();
            TextAppearance.DisplayWithColor(TextAppearance.WrapText("Konto zostało utworzone pomyślnie"), ConsoleColor.Green);
        }

        public string ShowEmailCreator()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Podaj swój adres e-mail"));
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Z - zrezygnuj z tworzenia konta"));
            TextAppearance.DisplayBorder();
            Console.Write("E-mail: ");
            string email = Console.ReadLine();

            if(email.Replace(" ", "") == "")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("E-mail nie może być pusty"), ConsoleColor.Red);
                return ShowEmailCreator();
            }

            if(email.Trim().ToUpper() == "Z")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Konto nie zostało utworzone"), ConsoleColor.Green);
                Program.Main(null);
                return null;
            }

            try
            {
                MailAddress mail = new MailAddress(email);
            }
            catch(FormatException)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Nieprawidłowy adres e-mail"), ConsoleColor.Red);
                return ShowEmailCreator();
            }

            ObservableCollection<User> users = Program.Hibernate.Query<User>("from User where Email = '" + email + "'");
            if(users.Count != 0)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Istnieje już konto z podanym adresem"), ConsoleColor.Red);
                return ShowEmailCreator();
            }

            return email;
        }

        public string ShowLoginCreator()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Wybierz nazwę użytkownika"));
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Z - zrezygnuj z tworzenia konta"));
            TextAppearance.DisplayBorder();
            Console.Write("Login: ");
            string login = Console.ReadLine();

            if (login.Replace(" ", "") == "")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Login nie może być pusty"), ConsoleColor.Red);
                return ShowLoginCreator();
            }

            if (login.Trim().ToUpper() == "Z")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Konto nie zostało utworzone"), ConsoleColor.Green);
                Program.Main(null);
                return null;
            }

            ObservableCollection<User> users = Program.Hibernate.Query<User>("from User where Login = '" + login + "'");
            if (users.Count != 0)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Istnieje już konto z podanym login"), ConsoleColor.Red);
                return ShowLoginCreator();
            }

            return login;
        }

        public string ShowPasswordCreator()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Utwórz hasło"));
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Z - zrezygnuj z tworzenia konta"));
            TextAppearance.DisplayBorder();
            Console.Write("Hasło: ");
            string password = TextAppearance.ReadPassword('*');

            if (password.Replace(" ", "") == "")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Hasło nie może być puste"), ConsoleColor.Red);
                return ShowPasswordCreator();
            }

            if (password.Trim().ToUpper() == "Z")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Konto nie zostało utworzone"), ConsoleColor.Green);
                Program.Main(null);
                return null;
            }

            if (password.Length < 8)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Hasło musi mieć co najmniej 8 znaków"), ConsoleColor.Red);
                return ShowPasswordCreator();
            }

            return password;
        }

        public void SendEmail(string recipent, string userName)
        {
            try
            {
                var smtpClient = new SmtpClient("smtp.gmail.com")
                {
                    Port = 587,
                    Credentials = new NetworkCredential("noreply.surveyapp@gmail.com", "SecurePassW"),
                    EnableSsl = true,
                };

                smtpClient.Send("noreply.surveyapp@gmail.com", recipent, "Witamy w aplikacji SurveyApp!", "Witaj, " + userName + "!\n\nCieszymy się, że dołączyłeś/aś do naszej społeczności oraz życzmy miłego ankietowania!\n\nPozdrawiamy,\nZespół SurveyApp");
            }
            catch { }
        }
    }
}
