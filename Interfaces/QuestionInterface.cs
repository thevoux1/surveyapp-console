﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Interfaces
{
    class QuestionInterface
    {
        public Question CurrentQuestion { get; set; }
        public int QuestionNumber { get; set; }
        public ObservableCollection<Answer> Answers { get; set; }

        public QuestionInterface(Question question, int questionNumber)
        {
            CurrentQuestion = question;
            QuestionNumber = questionNumber;
            Answers = Program.Hibernate.Query<Answer>("from Answer where Question = " + CurrentQuestion.ID);
        }

        public QuestionInterface() { }

        public Answer DisplayQuestion()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("#" + QuestionNumber + " " + CurrentQuestion.Content));

            if (CurrentQuestion.Type == 0)
                return DisplayClosedAnswers();
            else if (CurrentQuestion.Type == 1)
                return DisplayOpenedAnswer();
            else
                return DisplayRangedAnswers();
        }

        public Answer DisplayClosedAnswers()
        {
            string[] answersStrings = new string[Answers.Count + 1];
            answersStrings[Answers.Count] = "  Z - zrezygnuj z udziału w ankiecie";
            int answerIndex = 0;
            char answerCounter = 'a';

            foreach (Answer answer in Answers)
                answersStrings[answerIndex++] = "  " + answerCounter++ + ") " + answer.Content;

            string choice = TextAppearance.DisplayWithBorders(answersStrings);
            if(choice == "Z")
                return null;

            if (choice.Length == 2 && choice[1] == ')')
                choice = choice[0].ToString();

            char c = ' ';
            if (Char.TryParse(choice, out c))
            {
                if (c >= 'A' && c < Char.ToUpper(answerCounter))
                {
                    return Answers[c - 65];
                }
                else
                {
                    Console.Clear();
                    TextAppearance.DisplayWithColor("Odpowiedź spoza zakresu", ConsoleColor.Red);
                    return DisplayQuestion();
                }
            }
            else
            {
                Console.Clear();
                TextAppearance.DisplayWithColor("Nie rozpoznano odpowiedzi", ConsoleColor.Red);
                return DisplayQuestion();
            }
        }

        public Answer DisplayOpenedAnswer()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Z - zrezygnuj z udziału w ankiecie"));
            TextAppearance.DisplayBorder();
            Console.Write("Odpowiedź: ");
            string openedAnswer = Console.ReadLine().Trim().ToLower();

            if (openedAnswer.Replace(" ", "") == "")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor("Odpowiedź nie może być pusta", ConsoleColor.Red);
                return DisplayQuestion();
            }

            if (openedAnswer == "z")
                return null;

            foreach (Answer answer in Answers)
            {
                if (answer.Content == openedAnswer)
                    return answer;
            }

            Answer newAnswer = new Answer()
            {
                Content = openedAnswer,
                Question = CurrentQuestion
            };

            return newAnswer;
        }

        public Answer DisplayRangedAnswers()
        {
            string choice = TextAppearance.DisplayWithBorders(new string[] { "1 - zdecydowanie się zgadzam", "2 - zgadzam się", "3 - nie wiem", "4 - nie zgadzam się", "5 - zdecydowanie się nie zgadzam", "Z - zrezygnuj z udziału w ankiecie" });

            if (choice == "Z")
                return null;

            int optionNumber;
            if (Int32.TryParse(choice, out optionNumber))
            {
                if (optionNumber >= 1 && optionNumber <= 5)
                {
                    string textValue;

                    if (optionNumber == 1)
                        textValue = "zdecydowanie się nie zgadzam";
                    else if (optionNumber == 2)
                        textValue = "nie zgadzam się";
                    else if (optionNumber == 3)
                        textValue = "nie wiem";
                    else if (optionNumber == 4)
                        textValue = "zgadzam się";
                    else
                        textValue = "zdecydowanie się zgadzam";

                    foreach (Answer answer in Answers)
                    {
                        if (answer.Content == textValue)
                            return answer;
                    }

                    Answer newAnswer = new Answer()
                    {
                        Content = textValue,
                        Question = CurrentQuestion
                    };

                    return newAnswer;
                }
                else
                {
                    Console.Clear();
                    TextAppearance.DisplayWithColor("Wybrana odpowiedź jest spoza zakresu", ConsoleColor.Red);
                    return DisplayQuestion();
                }
            }
            else
            {
                Console.Clear();
                TextAppearance.DisplayWithColor("Wybrana odpowiedź nie jest wartością liczbową", ConsoleColor.Red);
                return DisplayQuestion();
            }
        }
    }
}
