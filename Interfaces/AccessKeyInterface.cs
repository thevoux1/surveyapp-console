﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Interfaces
{
    class AccessKeyInterface
    {
        private readonly Random _random = new Random();

        public void Show()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Podaj kod dostępu"));
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("P - powrót do strony głównej"));
            TextAppearance.DisplayBorder();
            Console.Write("Kod dostępu: ");
            string accesKey = Console.ReadLine();

            if(accesKey.Trim().ToUpper() == "P")
            {
                Console.Clear();
                Program.Main(null);
                return;
            }

            Survey survey = CheckAccessKey(accesKey);
            if (survey == null)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor("Niepoprawny kod dostępu", ConsoleColor.Red);
                Show();
            }
            else
            {
                Console.Clear();
                SurveyInterface surveyInterface = new SurveyInterface(survey);
                surveyInterface.ShowSurveyDetails();
            }
        }

        public Survey CheckAccessKey(string accesKey)
        {
            ObservableCollection<Survey> survey = Program.Hibernate.Query<Survey>("from Survey where AccessKey = '" + accesKey + "'");
            if (survey.Count == 0)
                return null;
            else
                return survey[0];
        }

        public string GenerateKey()
        {
            StringBuilder passwordBuilder = new StringBuilder();
            passwordBuilder.Append(RandomString(1, true));
            passwordBuilder.Append(RandomString(2));
            passwordBuilder.Append(RandomNumber(11, 99));
            passwordBuilder.Append(RandomString(2, true));
            passwordBuilder.Append(RandomNumber(0, 9));
            passwordBuilder.Append(RandomString(1));
            passwordBuilder.Append(RandomString(1, true));
            passwordBuilder.Append(RandomNumber(11, 99));

            return passwordBuilder.ToString();
        }

        private string RandomString(int size, bool lowerCase = false)
        {
            var builder = new StringBuilder(size);
            char offset = lowerCase ? 'a' : 'A';
            const int lettersOffset = 26;

            for (var i = 0; i < size; i++)
            {
                var @char = (char)_random.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }

            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }

        private int RandomNumber(int min, int max)
        {
            return _random.Next(min, max);
        }
    }
}
