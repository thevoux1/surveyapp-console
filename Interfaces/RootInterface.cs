﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Interfaces
{
    class RootInterface
    {
        ObservableCollection<Survey> publicSurveys;

        public void ShowPublicSurveys()
        {
            TextAppearance.DisplayBorder();
            publicSurveys = Program.Hibernate.Query<Survey>("from Survey where AccessKey is null");
            Console.WriteLine(TextAppearance.WrapText("Aktualne, publiczne ankiety:"));

            int counter = 1;
            foreach (Survey survey in publicSurveys)
            {
                if(survey.StartDate <= DateTime.Now && survey.EndDate >= DateTime.Now && survey.User != null)
                    Console.WriteLine(counter++ + ") " + survey.Name);
            }
        }

        public void RootChoose()
        {
            string mainChoice = TextAppearance.DisplayWithBorders(new string[] { "W - wpisz kod dostępu", "L - logowanie", "R - rejestracja", "Nr ankiety - przejście do ankiety", "Z - zamknij program" });

            if(mainChoice == "W")
            {
                AccessKeyInterface accessKeyInterface = new AccessKeyInterface();
                accessKeyInterface.Show();
            }
            else if (mainChoice == "L")
            {
                LoggingInterface loggingInterface = new LoggingInterface();
                loggingInterface.ShowLogIn();
            }
            else if (mainChoice == "R")
            {
                RegistrationInterface registrationInterface = new RegistrationInterface();
                registrationInterface.Show();
                Program.Main(null);
            }
            else if (mainChoice == "Z")
            {
                Program.DisplayAnimation();
                Environment.Exit(0);
            }
            else
            {
                int number = 0;
                if (!Int32.TryParse(mainChoice, out number))
                {
                    TextAppearance.DisplayWithColor("Nie rozpoznano opcji", ConsoleColor.Red);
                    Program.Main(null);
                }
                else
                {
                    if (number < 1 || number > publicSurveys.Count)
                    {
                        TextAppearance.DisplayWithColor("Nie istnieje ankieta o takim numerze", ConsoleColor.Red);
                        Program.Main(null);
                    }
                    else
                    {
                        SurveyInterface surveyInterface = new SurveyInterface(publicSurveys[number - 1]);
                        surveyInterface.ShowSurveyDetails();
                        Program.Main(null);
                    }
                }
            }
        }
    }
}
