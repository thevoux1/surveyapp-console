﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Interfaces
{
    class AnswersCreatorInterface
    {
        private List<Answer> answers;
        public string SurveyName { get; set; }
        public Question CurrentQuestion { get; set; }
        public int QuestionNumber { get; set; }

        public AnswersCreatorInterface(string surveyName, Question question, int questionNumber)
        {
            answers = new List<Answer>();
            SurveyName = surveyName;
            CurrentQuestion = question;
            QuestionNumber = questionNumber;
        }

        public List<Answer> CreateAnswer()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Ankieta \"" + SurveyName + "\""));
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Pytanie #" + QuestionNumber + " \"" + CurrentQuestion.Content + "\""));
            TextAppearance.DisplayBorder();
            char answerCounter = 'a';

            if (answers.Count != 0)
            {
                Console.WriteLine(TextAppearance.WrapText("Dodane odpowiedzi:"));

                foreach (Answer answer in answers)
                    Console.WriteLine(TextAppearance.WrapText(answerCounter++ + ") " + answer.Content));

                TextAppearance.DisplayBorder();
            }

            Console.WriteLine(TextAppearance.WrapText("W - zakończ dodawanie odpowiedzi"));
            Console.WriteLine(TextAppearance.WrapText("Z - zrezygnuj z tego pytania"));
            TextAppearance.DisplayBorder();

            Console.Write(answerCounter + ") ");
            string content = Console.ReadLine();

            if (content.Replace(" ", "") == "")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Odpowiedź nie może być pusta"), ConsoleColor.Red);
                return CreateAnswer();
            }
            else if(content.Trim().ToUpper() == "W")
            {
                if (answers.Count < 2)
                {
                    Console.Clear();
                    TextAppearance.DisplayWithColor(TextAppearance.WrapText("Dodaj co najmniej dwie odpowiedzi"), ConsoleColor.Red);
                    return CreateAnswer();
                }
                else
                {
                    Console.Clear();
                    return answers;
                }
            }
            else if (content.Trim().ToUpper() == "Z")
            {
                Console.Clear();
                return null;
            }
            else
            {
                Answer newAnswer = new Answer()
                {
                    Content = content,
                    Question = CurrentQuestion
                };

                answers.Add(newAnswer);
                Console.Clear();
                return CreateAnswer();
            }
        }
    }
}
