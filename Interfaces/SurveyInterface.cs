﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Interfaces
{
    class SurveyInterface
    {
        private Survey survey;
        public SurveyInterface(Survey survey)
        {
            this.survey = survey;
        }

        public void ShowSurveyDetails()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Nazwa: " + survey.Name));
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Data rozpoczęcia: " + survey.StartDate.ToString("F")));
            Console.WriteLine(TextAppearance.WrapText("Data zakończenia: " + survey.EndDate.ToString("F")));
            Console.WriteLine(TextAppearance.WrapText("Rodzaj ankiety: " + (survey.AccessKey == null ? "publiczna" : "prywatna")));
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Opis: " + survey.Description));
            string choice = TextAppearance.DisplayWithBorders(new string[] { "1 - Rozpocznij", "2 - Strona główna" });
            if(choice == "1")
            {
                if(survey.StartDate > DateTime.Now)
                {
                    Console.Clear();
                    TextAppearance.DisplayWithColor(TextAppearance.WrapText("Ankieta jeszcze się nie rozpoczęła"), ConsoleColor.Red);
                    ShowSurveyDetails();
                }
                else if(survey.EndDate < DateTime.Now)
                {
                    Console.Clear();
                    TextAppearance.DisplayWithColor(TextAppearance.WrapText("Ankieta już się zakończyła"), ConsoleColor.Red);
                    ShowSurveyDetails();
                }
                else if (survey.User == null)
                {
                    Console.Clear();
                    TextAppearance.DisplayWithColor(TextAppearance.WrapText("Ankieta została usunięta"), ConsoleColor.Red);
                    ShowSurveyDetails();
                }

                StartSurvey();
            }
            else if (choice == "2")
            {
                Program.Main(null);
            }
            else
            {
                TextAppearance.DisplayWithColor("Nie rozpoznano wyboru", ConsoleColor.Red);
                ShowSurveyDetails();
            }
        }

        public void StartSurvey()
        {
            ObservableCollection<Question> questions = Program.Hibernate.Query<Question>("from Question where Survey = " + survey.ID);
            List<Answer> answers = new List<Answer>();
            QuestionInterface questionInterface;

            int questionCounter = 1;
            foreach (Question question in questions)
            {
                Console.Clear();
                questionInterface = new QuestionInterface(question, questionCounter++);
                Answer newAnswer = questionInterface.DisplayQuestion();
                if(newAnswer != null)
                {
                    newAnswer.Selections++;
                    answers.Add(newAnswer);
                }
                else
                {
                    Console.Clear();
                    TextAppearance.DisplayWithColor(TextAppearance.WrapText("Pomyślnie opuszczono ankietę"), ConsoleColor.Green);
                    return;
                }
            }

            Program.Hibernate.AddAnswers(answers);

            Console.Clear();
            TextAppearance.DisplayWithColor(TextAppearance.WrapText("Pomyślnie dodano odpowiedzi"), ConsoleColor.Green);
        }
    }
}
