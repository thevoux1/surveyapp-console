﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Interfaces
{
    class ManageAccountInterface
    {
        public User CurrentUser { get; set; }
        private string newPassword;

        public ManageAccountInterface(User user)
        {
            CurrentUser = user;
        }

        public void RecoverPassword()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Podaj swój adres e-mail:"));
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("P - powrót do strony głównej"));
            TextAppearance.DisplayBorder();
            Console.Write("E-mail: ");
            string email = Console.ReadLine();

            if(email.Trim().ToUpper() == "P")
            {
                Console.Clear();
                Program.Main(null);
                return;
            }
            else if(email.Replace(" ", "") == "")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("E-mail nie może być pusty"), ConsoleColor.Red);
                RecoverPassword();
                return;
            }

            ObservableCollection<User> users = Program.Hibernate.Query<User>("from User where Email = '" + email + "'");
            if(users.Count == 0)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Błędny email lub takie konto nie istnieje"), ConsoleColor.Red);
                RecoverPassword();
                return;
            }

            SendEmail(users[0]);
            Console.Clear();
            TextAppearance.DisplayWithColor(TextAppearance.WrapText("Hasło zostało wysłane na Twój adres e-mail"), ConsoleColor.Green);
            Program.Main(null);
        }

        public void ChangePassword()
        {
            if (!TypeCurrentPassword())
            {
                Console.Clear();
                return;
            }

            Console.Clear();
            if (!TypeNewPassword())
            {
                Console.Clear();
                return;
            }

            Console.Clear();
            if (!ConfirmNewPassword())
            {
                Console.Clear();
                return;
            }

            CurrentUser.Password = newPassword;
            Program.Hibernate.UpdateData<User>(CurrentUser);
            Console.Clear();
            TextAppearance.DisplayWithColor(TextAppearance.WrapText("Hasło zostało zmienione pomyślnie"), ConsoleColor.Green);
        }

        private bool TypeCurrentPassword()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Podaj swoje aktualne hasło"));
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("P - powrót do panelu użytkownika"));
            TextAppearance.DisplayBorder();
            Console.Write("Aktualne hasło: ");
            string password = TextAppearance.ReadPassword('*');

            if (password.Trim().ToUpper() == "P")
                return false;

            if (password == CurrentUser.Password)
                return true;
            else
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Nieprawidłowe hasło"), ConsoleColor.Red);
                return TypeCurrentPassword();
            }
        }

        private bool TypeNewPassword()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Podaj nowe hasło"));
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("P - powrót do panelu użytkownika"));
            TextAppearance.DisplayBorder();
            Console.Write("Nowe hasło: ");
            string password = TextAppearance.ReadPassword('*');

            if (password.Trim().ToUpper() == "P")
                return false;

            if (password.Replace(" ", "") == "")
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Hasło nie może być puste"), ConsoleColor.Red);
                return TypeNewPassword();
            }
            else if (password == CurrentUser.Password)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Hasło nie może być takie same jak poprzednie"), ConsoleColor.Red);
                return TypeNewPassword();
            }
            else if (password.Length < 8)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Hasło musi mieć co najmniej 8 znaków"), ConsoleColor.Red);
                return TypeNewPassword();
            }

            newPassword = password;
            return true;
        }

        private bool ConfirmNewPassword()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Podaj nowe hasło ponownie"));
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("P - powrót do panelu użytkownika"));
            TextAppearance.DisplayBorder();
            Console.Write("Nowe hasło: ");
            string password = TextAppearance.ReadPassword('*');

            if (password.Trim().ToUpper() == "P")
                return false;

            if (password != newPassword)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Hasła nie są ze sobą zgodne"), ConsoleColor.Red);
                return ConfirmNewPassword();
            }

            return true;
        }

        public void SendEmail(User user)
        {
            try
            {
                var smtpClient = new SmtpClient("smtp.gmail.com")
                {
                    Port = 587,
                    Credentials = new NetworkCredential("noreply.surveyapp@gmail.com", "SecurePassW"),
                    EnableSsl = true,
                };

                smtpClient.Send("noreply.surveyapp@gmail.com", user.Email, "Zapomniane hasło", "Witaj, " + user.Login + "!\n\nHasło do Twojego konta to " + user.Password + "\nZapisz je sobie w bezpiecznym miejscu.\n\nPozdrawiamy,\nZespół SurveyApp");
            }
            catch { }
        }

    }
}
