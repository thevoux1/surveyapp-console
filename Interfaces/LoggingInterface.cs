﻿using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Interfaces
{
    class LoggingInterface
    {
        public User CheckLogging(string login, string password)
        {
            ObservableCollection<User> user = Program.Hibernate.Query<User>("from User where Login = '" + login + "'");
            if (user.Count == 0 || user[0].Password != password)
                return null;
            else
                return user[0];
        }

        public void ShowLogIn()
        {
            TextAppearance.DisplayBorder();
            Console.WriteLine(TextAppearance.WrapText("Logowanie do panelu użytkownika"));
            TextAppearance.DisplayBorder();
            Console.Write("Login: ");
            string login = Console.ReadLine();
            Console.Write("Hasło: ");
            string password = TextAppearance.ReadPassword('*');
            User user = CheckLogging(login, password);

            if (user == null)
            {
                Console.Clear();
                TextAppearance.DisplayWithColor("Nieprawidłowe dane logowania", ConsoleColor.Red);
                ShowErrorLogIn();
            }
            else
            {
                Console.Clear();
                TextAppearance.DisplayWithColor("Pomyślnie zalogowano", ConsoleColor.Green);
                UserInterface userInterface = new UserInterface(user);
                userInterface.Show();
            }
        }

        public void ShowErrorLogIn()
        {
            string logChoice = TextAppearance.DisplayWithBorders(new string[] { "1 - Spróbuj ponownie", "2 - Przypomnij hasło", "3 - Strona główna" });
            if (logChoice == "1")
                ShowLogIn();
            else if (logChoice == "2")
            {
                ManageAccountInterface manageAccountInterface = new ManageAccountInterface(null);
                manageAccountInterface.RecoverPassword();
            }
            else if (logChoice == "3")
                Program.Main(null);
            else
            {
                TextAppearance.DisplayWithColor("Nie rozpoznano wyboru", ConsoleColor.Red);
                ShowErrorLogIn();
            }
        }
    }
}
