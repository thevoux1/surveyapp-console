﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Models
{
    public class Survey
    {
        public Survey() { }
        public virtual int ID { get; set; }
        public virtual User User { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string AccessKey { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
    }
}
