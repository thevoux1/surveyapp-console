﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace surveyapp.Models
{
    public class Question
    {
        public Question() { }
        public virtual int ID { get; set; }
        public virtual Survey Survey { get; set; }
        public virtual string Content { get; set; }
        public virtual int Type { get; set; }
    }
}
