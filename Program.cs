﻿using surveyapp.Interfaces;
using surveyapp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static surveyapp.Interfaces.LoggingInterface;

namespace surveyapp
{
    class Program
    {
        public static NHibernate Hibernate;

        public static void Main(string[] args)
        {
            if(Hibernate == null)
            {
                if (!ConnectWithDataBase())
                    return;

                DisplayAnimation();
            }

            RootInterface rootInterface = new RootInterface();
            rootInterface.ShowPublicSurveys();
            rootInterface.RootChoose();
        }

        public static bool ConnectWithDataBase()
        {
            try
            {
                Hibernate = new NHibernate();
            }
            catch (Exception e)
            {
                TextAppearance.DisplayBorder();
                TextAppearance.DisplayWithColor(TextAppearance.WrapText("Nie udało się nawiązać połączenia z bazą danych. " + e.Message), ConsoleColor.Red);
                TextAppearance.DisplayBorder();
                Console.ReadKey();
                return false;
            }

            return true;
        }

        public static void DisplayAnimation()
        {
            DisplayLogoColors(ConsoleColor.Green, ConsoleColor.Blue, 0.5);
            DisplayLogoColors(ConsoleColor.Blue, ConsoleColor.Green, 0.5);
            DisplayLogoColors(ConsoleColor.Green, ConsoleColor.Blue, 0.5);
            DisplayLogoColors(ConsoleColor.Blue, ConsoleColor.Green, 0.5);
            DisplayLogoColors(ConsoleColor.Green, ConsoleColor.Blue, 0.5);
            DisplayLogoColors(ConsoleColor.Blue, ConsoleColor.Green, 0.5);
        }

        public static void DisplayLogoColors(ConsoleColor foreground, ConsoleColor background, double seconds)
        {
            Console.ForegroundColor = foreground;
            Console.BackgroundColor = background;
            DisplayLogo();
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.BackgroundColor = ConsoleColor.Black;
            Thread.Sleep((int)(seconds * 1000));
            Console.Clear();
        }

        public static void DisplayLogo()
        {
            Console.WriteLine(@"         ___ _   _ _ ____   _____ _   _         ");
            Console.WriteLine(@"        / __| | | | '__\ \ / / _ \ | | |        ");
            Console.WriteLine(@"        \__ \ |_| | |   \ V /  __/ |_| |        ");
            Console.WriteLine(@"        |___/\__,_|_|    \_/ \___|\__, |        ");
            Console.WriteLine(@"               __ _ _ __  _ __     __/ |        ");
            Console.WriteLine(@"              / _` | '_ \| '_ \   |___/         ");
            Console.WriteLine(@"             | (_| | |_) | |_) |                ");
            Console.WriteLine(@"              \__,_| .__/| .__/                 ");
            Console.WriteLine(@"                   |_|   |_|                    ");
            Console.WriteLine(@"                                                ");
        }
    }
}
